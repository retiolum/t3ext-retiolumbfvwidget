<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

call_user_func(function($extensionKey) {
	// Define plugin (legacy).
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
		'Retiolum.' . $extensionKey,
		'bfvwidget',
		[
			'Widget' => 'legacy',
        ],
		[
        ]
	);

    // Register icons.
    /** @var \TYPO3\CMS\Core\Imaging\IconRegistry $iconRegistry */
    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    $iconRegistry->registerIcon(
        'tx_retiolumbfvwidget_plugin',
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        [
            'source' => 'EXT:' . $extensionKey . '/Resources/Public/Icons/plugin.svg',
        ]
    );

    // Register PageTS settings.
    $pageTS = <<<EOT
mod.wizards.newContentElement.wizardItems.plugins.elements.retiolumbfvwidget-plugin {
	iconIdentifier = tx_retiolumbfvwidget_plugin
	title = LLL:EXT:retiolumbfvwidget/Resources/Private/Language/locallang_be.xlf:bfvwidget_title
	description = LLL:EXT:retiolumbfvwidget/Resources/Private/Language/locallang_be.xlf:bfvwidget_description
	tt_content_defValues {
		CType = list
		list_type = retiolumbfvwidget_bfvwidget
	}
}
EOT;
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig($pageTS);
}, 'retiolumbfvwidget');