<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

call_user_func(function($extensionKey) {
	// Register plugin and add Flexform (legacy).
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
		'Retiolum.' . $extensionKey,
		'bfvwidget',
		'LLL:EXT:' . $extensionKey . '/Resources/Private/Language/locallang_be.xlf:bfvwidget_title'
	);
	$pluginSignature = strtolower($extensionKey . '_bfvwidget');
	$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $extensionKey . '/Configuration/FlexForms/flexform_legacy.xml');
}, 'retiolumbfvwidget');
