<?php

########################################################################
# Extension Manager/Repository config file for ext "retiolumbfvwidget".
#
# Auto generated 27-07-2012 11:09
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = [
	'title' => 'BFV widget',
	'description' => 'This extension provides a simple plugin to display the BFV (Bayerischer Fußball-Verband) widget; this way you can show the latest results, current table and upcoming matches of your football team from Bavaria.',
	'category' => 'plugin',
	'author' => 'Thomas Off',
	'author_email' => 'thomas.off@retiolum.de',
	'state' => 'stable',
	'uploadfolder' => FALSE,
	'clearCacheOnLoad' => FALSE,
	'version' => '1.2.0',
	'constraints' => [
		'depends' => [
			'typo3' => '8.7.0-9.5.99',
        ],
		'conflicts' => [
        ],
		'suggests' => [
        ],
    ],
	'autoload' => [
		'psr-4' => [
			'Retiolum\\Retiolumbfvwidget\\' => 'Classes',
        ],
    ],
];
