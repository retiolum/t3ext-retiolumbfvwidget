<?php
defined('TYPO3_MODE') || die();

// Add static TypoScript template.
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('retiolumbfvwidget', 'Configuration/TypoScript', 'BFV widgets');
